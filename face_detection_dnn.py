import numpy as np
import argparse
import cv2
import matplotlib.pyplot as plt

def convertToRGB(image):
	return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# construct and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
ap.add_argument("-p", "--prototxt", required=True,
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
	help="minimum probability to filter weak detections")
args = vars(ap.parse_args())

# load serialized model from disk
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# construct an input blob for the image, resize to 300x300 pixels, normalize
image = cv2.imread(args["image"])
(h, w) = image.shape[:2]
blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0,
	(300, 300), (104.0, 177.0, 123.0))

# pass the blob through the network
net.setInput(blob)
detected_faces = net.forward()

# loop over the detected faces
for i in range(0, detected_faces.shape[2]):
	# extract the confidence (i.e., probability)
	confidence = detected_faces[0, 0, i, 2]

	# filter out weak detected_faces by the minimum confidence
	if confidence > args["confidence"]:
		# compute the (x, y)-coordinates of the bounding box
		box = detected_faces[0, 0, i, 3:7] * np.array([w, h, w, h])
		(startX, startY, endX, endY) = box.astype("int")
 
		# draw the bounding box with its probabilty text
		text = "{:.2f}%".format(confidence * 100)
		y = startY - 10 if startY - 10 > 10 else startY + 10
		cv2.rectangle(image, (startX, startY), (endX, endY),
			(0, 255, 0), 2)
		cv2.putText(image, text, (startX, y),
			cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 255, 0), 2)

# show the output image
plt.imshow(convertToRGB(image))
plt.show()