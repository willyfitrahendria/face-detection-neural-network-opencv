# Face Detection Neural Network OpenCV

Deep learning-based face detector in OpenCV

Python 3.7.3

OpenCV 4.1.1-dev

Usage
---

>  python face_detection_dnn.py --image single.jpg --prototxt deploy.prototxt.txt --model res10_300x300_ssd_iter_140000.caffemodel

Result
---
**single face**
![](./output/single_result.jpg)
**multi face**
![](./output/group_result.jpg)